/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function validateUserName()
{
    var name = document.getElementById("userName").value;
    var password = document.getElementById("userPass").value;
    var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;
    if (name.length < 3) {
        document.getElementById("unameMsg").innerHTML = "Enter a valid name";
        userName.focus();
        return false;
    } else if (!regularExpression.test(password)) {
        document.getElementById("passwordMsg").innerHTML = "Password must contain at least one number and one special charter";
        userPass.focus();
        return false;
    }

}
