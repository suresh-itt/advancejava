/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author suresh.sen
 */
public class RegisterDemo extends HttpServlet {

    static int flag = 1;
/*Validation for credential*/
    public void validateField(String name, String password, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        if (password.length() < 7 || password.equals("")) {
            out.println("<h1>Password field is Empty  or it should have atleast 6 correct</h1>");
            flag = 0;

        }
        if (name.length() < 3) {
            out.println("<h1>Enter a valid Name</h1>");
            flag = 0;
        } else {
            flag = 1;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String Email = request.getParameter("email");
        String Name = request.getParameter("uname");
        String Password = request.getParameter("pwd");
        String Address = request.getParameter("address");
        String Gender = request.getParameter("optradio");
        String State = request.getParameter("state");
        String City = request.getParameter("city");

        validateField(Name, Password, response);//calling validation method

        if (flag == 1) {

            try {

                Class.forName("com.mysql.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "1234");
                String sel = "SELECT * FROM employee  where email ='" + Email + "'";
                PreparedStatement ps1 = con.prepareCall(sel);
                ResultSet rs = ps1.executeQuery(sel);

                if (rs.next()) {
                    out.println("This email is already register");//cehcking mailid if already exist 
                    out.print(rs.getString("email"));

                } else {
                    /*inserting into database */
                    String sql = "Insert into employee (email,name,password,address,gender,state,city) values (?,?,?,?,?,?,?)";
                    PreparedStatement ps = con.prepareCall(sql);
                    ps.setString(1, Email);
                    ps.setString(2, Name);
                    ps.setString(3, Password);
                    ps.setString(4, Address);
                    ps.setString(5, Gender);
                    ps.setString(6, State);
                    ps.setString(7, City);
                    int k = ps.executeUpdate();
                    
                    if (k > 0) {
                        //   out.println("inside if k>0");
                        RequestDispatcher rd = request.getRequestDispatcher("WelcomeHome");
                        rd.include(request, response);
                        // out.println("<h2>Your Data is successfully stored in Database </h2><h3>As shown in the Table</h3>");
                    } else {
                        RequestDispatcher rd = request.getRequestDispatcher("signup.html");
                        rd.forward(request, response);
                        //out.println("Sorry fill right value in the form");
                    }
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(RegisterDemo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(RegisterDemo.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
