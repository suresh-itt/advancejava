/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author suresh.sen
 */
public class SessionDemo extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            Class.forName("com.mysql.jdbc.Driver");

            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "1234");

            String name = request.getParameter("email");
            String password = request.getParameter("pwd");

            String sql = "SELECT * FROM employee  where email ='" + name + "'";
           
            PreparedStatement ps = con.prepareStatement(sql);

            ResultSet rs = ps.executeQuery(sql);

            String storedEmail;
            String storedPassword;
            
            if (rs.next()) {
                storedEmail = rs.getString("email");
                storedPassword = rs.getString("password");
                 /*validating a valid user to login */
                if (name.equals(storedEmail) && password.equals(storedPassword)) {
                    HttpSession session = request.getSession(true);
                    session.setAttribute("uname", name);
                    session.setAttribute("upass", password);
                    response.sendRedirect("ViewDetails"); //redirection to serlvlet to show stored session attribute

                }

            } else {
                RequestDispatcher rd=request.getRequestDispatcher("login.html");
                rd.include(request, response);
                out.println("<h3>Sorry !!! Your Credential is not correct</h3>");
            }

            out.close();
        } catch (Exception exp) {
            System.out.println(exp.getMessage());
        }

    }

}
