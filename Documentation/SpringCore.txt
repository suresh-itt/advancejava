*Ioc Container:
 Spring IoC is the mechanism to achieve loose-coupling between Objects dependencies. To achieve 
 loose coupling and dynamic binding of the objects at runtime, objects dependencies are injected
 by other assembler objects. Spring IoC container is the program that injects dependencies into
 an object and make it ready for our use.

 BeanFactory is the root interface of Spring IoC container. ApplicationContext is the child interface
 of BeanFactory interface that provide Spring AOP features,

Some of the useful ApplicationContext implementations that we use are;

# AnnotationConfigApplicationContext: If we are using Spring in standalone java applications and using
annotations for Configuration, then we can use this to initialize the container and get the bean objects.

# ClassPathXmlApplicationContext: If we have spring bean configuration xml file in standalone application, 
then we can use this class to load the file and get the container object.

# FileSystemXmlApplicationContext: This is similar to ClassPathXmlApplicationContext except that the xml 
configuration file can be loaded from anywhere in the file system.

# AnnotationConfigWebApplicationContext and XmlWebApplicationContext for web applications.

*Spring bean
 Spring Bean is nothing special, any object in the Spring framework that we initialize through Spring 
 container is called Spring Bean. Any normal Java POJO class can be a Spring Bean if it�s configured to
 be initialized via container by providing configuration metadata information.

* Bean Scope
 1.singleton � Only one instance of the bean will be created for each container.
 	       This is the default scope for the spring beans. While using this scope, make sure
               bean doesn�t have shared instance variables otherwise it might lead to data inconsistency
               issues.

 2.prototype � A new instance will be created every time the bean is requested.

 3.request � This is same as prototype scope, however it�s meant to be used for web applications.
             A new instance of the bean will be created for each HTTP request.

 4.session � A new bean will be created for each HTTP session by the container.

 5.global-session � This is used to create global session beans for Portlet applications.

* Spring bean configuration

  1.Annotation Based Configuration � By using @Service or @Component annotations. Scope details 
    can be provided with @Scope annotation.

  2.XML Based Configuration � By creating Spring Configuration XML file to configure the beans. 
    If you are using Spring MVC framework, the xml based configuration can be loaded automatically
    by writing some boiler plate code in web.xml file.

  3.Java Based Configuration � Starting from Spring 3.0, we can configure Spring beans using java programs.
    Some important annotations used for java based configuration are @Configuration, @ComponentScan and @Bean.

* Spring architecture
  
 1.The Core Module: Provides the Dependency Injection (DI) feature which is the basic concept of the Spring 
   framework. This module contains the BeanFactory, an implementation of Factory Pattern which creates the 
   bean as per the configurations provided by the developer in an XML file.

 2.AOP Module: The Aspect Oriented Programming module allows developers to define method-interceptors and
   point cuts to keep the concerns apart. It is configured at run time so the compilation step is skipped. 
   It aims at declarative transaction management which is easier to maintain.

 3.DAO Module: This provides an abstraction layer to the low level task of creating a connection, releasing
   it etc. It also maintains a hierarchy of meaningful exceptions rather than throwing complicated error codes
    from specific database vendors. It uses AOP to manage transactions. Transactions can also be managed programmatically.

 4.ORM Module: Spring doesn�t provides its own ORM implementation but offers integrations with popular Object
   Relational mapping tools like Hibernate, iBATIS SQL Maps, Oracle TopLink and JPA etc.

 5.JEE Module: It also provides support for JMX, JCA, EJB and JMS etc. In lots of cases, JCA (Java EE Connect
   ion API) is much like JDBC, except where JDBC is focused on database JCA focus on connecting to legacy sy-
   stems.

 6.Web Module: Spring comes with MVC framework which eases the task of developing web applications. It also
   integrates well with the most popular MVC frameworks like Struts, Tapestry, JSF, Wicket etc.

 * Java Dependency Injection

   Java Dependency Injection design pattern allows us to remove the hard-coded dependencies and make our application loosely coupled,
   extendable and maintainable. We can implement dependency injection in java to move the dependency resolution from compile-time to runtime.


