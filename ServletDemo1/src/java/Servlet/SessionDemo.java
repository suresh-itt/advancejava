/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author suresh.sen
 */
public class SessionDemo extends HttpServlet
{  
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
         try
         {
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            String name = request.getParameter("email");
            String password = request.getParameter("pwd");
           if(name.equals("suresh")&& password.equals("12345"))
           {
            HttpSession session=request.getSession();
            session.setAttribute("uname",name);
            session.setAttribute("upass",password);
                response.sendRedirect("welcome.html");
           }
           else
           {
               RequestDispatcher rd = request.getRequestDispatcher("index.html");
               rd.include(request, response);
               
           }
            out.close();
        }
        catch(Exception exp)
        {
           System.out.println(exp);
        }

    }
   

   
    
}
