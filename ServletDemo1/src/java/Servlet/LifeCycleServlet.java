/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author suresh.sen
 */
public class LifeCycleServlet extends HttpServlet
{
    ServletConfig config = null;
    
    public void init(ServletConfig config) throws ServletException
    {
        this.config = config;
        System.out.println("This is a init method");
    }

    
    public ServletConfig getServletConfig()
    {
       return config;
    }

  
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException
    {
       response.setContentType("text/html");
       PrintWriter out = response.getWriter();
       String username = request.getParameter("email");
       String password = request.getParameter("pwd");
       
       if(username.equals("suresh")&&password.equals("1234"))
       {
           RequestDispatcher rd = request.getRequestDispatcher("welcome.html");
           rd.forward(request, response);
       }
       else
       {
           RequestDispatcher rd = request.getRequestDispatcher("demo.html");
           rd.forward(request, response);
       }
    }

   
    public void destroy()
    {
        System.out.println("servlet life cycle finished");
    }
      
    public String getServletInfo()
    {
        return "this is a information given by servlet info method";
    }

}
