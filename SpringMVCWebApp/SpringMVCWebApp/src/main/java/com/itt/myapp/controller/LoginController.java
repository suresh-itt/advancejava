/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.itt.myapp.domain.Login;
import com.itt.myapp.domain.Student;
import com.itt.myapp.service.StudentService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class LoginController 
{
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	StudentService studentService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("login");
		mav.addObject("login", new Login());
		return mav;
	}
/*Login Process take here*/
	@RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
	public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("login") Login login) 
        {
             logger.info("inside the login process");
		ModelAndView mav = null;
	    Student   student = studentService.validateUser(login);
                if (null != student) 
                {
               
                    List<Student> list = studentService.getUsersData();
                    mav = new ModelAndView("welcomepage");
                    mav.addObject("message","Welcome "+student.getFirstName()+" "+student.getLastName());
                    mav.addObject("list",list);
                    request.getSession().setAttribute("studentemail",login.getUsername());
                } 
                else 
                {
                mav = new ModelAndView("login");
                mav.addObject("message", "Username or Password is wrong!!");
                }
                return mav;
                    
        }
}