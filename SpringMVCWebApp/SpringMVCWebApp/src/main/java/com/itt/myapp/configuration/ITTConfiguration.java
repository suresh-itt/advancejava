package com.itt.myapp.configuration;

import com.itt.myapp.controller.HomeController;
import com.itt.myapp.dao.StudentDao;
import com.itt.myapp.dao.StudentDaoImpl;
import com.itt.myapp.service.StudentService;
import com.itt.myapp.service.StudentServiceImpl;
import com.itt.myapp.service.ValidatorService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.itt.myapp")
public class ITTConfiguration extends WebMvcConfigurerAdapter
{
     private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
   // @Autowired
    //private Environment env;

	@Bean(name = "HelloWorld")
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
        
    
    
	@Bean(name = "studentService")
	public StudentService studentService() 
        {
		return new StudentServiceImpl();
	}
        
        @Bean
        public DataSource dataSource() 
        {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/demo");
        dataSource.setUsername("root");
        dataSource.setPassword("suresh");
        return dataSource;
    }
        
        @Bean
        public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        return jdbcTemplate;
         }
        
        @Bean(name = "studentDao")
        public StudentDao studentDao()
        {
            return new StudentDaoImpl();
        }
        @Bean
        public ValidatorService validatorService()
        {
            return new ValidatorService();
        }

}