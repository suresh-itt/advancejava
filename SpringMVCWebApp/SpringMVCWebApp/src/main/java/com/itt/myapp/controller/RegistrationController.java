/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.controller;
import com.itt.myapp.domain.Student;
import com.itt.myapp.service.StudentService;
import com.itt.myapp.service.ValidatorService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class RegistrationController 
{
  private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
  @Autowired
  public StudentService studentService;
   @Autowired
  public  ValidatorService validatorService;
  
  @RequestMapping(value = "/register", method = RequestMethod.GET)
  public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) 
  {
    ModelAndView mav = new ModelAndView("register");
    mav.addObject("student", new Student());
    return mav;
  }
  /*Registration process */
  @RequestMapping(value = "/registerProcess", method = RequestMethod.POST)
  public ModelAndView registerProcess(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("student") Student student) 
  {
                ModelAndView mav = null;
                logger.info("inside validation method");
                
               if( validatorService.isValidName(student.getFirstName())&&validatorService.isValidName(student.getLastName()))
                {
                    logger.info("inside namechecker");
                  if(validatorService.isValidEmail(student.getEmail()))
                  {
                      logger.info("inside emailchecker");
                      if(validatorService.isValidPassword(student.getPassword()))
                      {
                          logger.info("inside password");
                          if(validatorService.isValidPhone(student.getPhone()))
                          {
                              logger.info("inside phone");
                            studentService.register(student);
                            List<Student> list=studentService.getUsersData();
                            mav = new ModelAndView("welcomepage");
                            mav.addObject("list",list);
                            mav.addObject("message","Welcome "+student.getFirstName()+" "+student.getLastName());
                          }
                          else
                          {
                              mav = new ModelAndView("register");
                              mav.addObject("phoneErrorMessage", "Enter a valid phone number atleast 10 digit");
                          }
                      }
                      else
                      {
                          mav = new ModelAndView("register");
                          mav.addObject("passwordErrorMessage", "Enter a valid password must greater than 6");
                      }
                  }
                  else
                  {
                      mav = new ModelAndView("register");
                      mav.addObject("emailErrorMessage", "Enter a valid Email Address");
                  }
               }
               else
               {
                    mav = new ModelAndView("register");
                    mav.addObject("nameErrorMessage", "Invalid Names in First Name or Last Name names must not bne null and not contains integer");
               }            
                return  mav;
  }
		
  
}
