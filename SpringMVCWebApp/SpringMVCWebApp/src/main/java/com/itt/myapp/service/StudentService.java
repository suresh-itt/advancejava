/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.service;

import com.itt.myapp.domain.*;
import java.util.List;

public interface StudentService 
{
	void register(Student student);
        Student validateUser(Login login);
        public List<Student> getUsersData();
}
