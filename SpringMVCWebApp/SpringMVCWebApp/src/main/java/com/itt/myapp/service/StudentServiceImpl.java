
package com.itt.myapp.service;

import com.itt.myapp.controller.HomeController;
import com.itt.myapp.dao.StudentDao;
import org.springframework.stereotype.Service;
import com.itt.myapp.domain.Login;
import com.itt.myapp.domain.Student;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Service("studentService")
public class StudentServiceImpl implements StudentService 
{
private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
    @Autowired
    StudentDao studentDao;
    
    @Override
    public void register(Student student) 
    {
        
        studentDao.register(student);
    }

    @Override
    public Student validateUser(Login login) 
    {
       return studentDao.validateUser(login);
    }

    @Override
    public List<Student> getUsersData() {
        return studentDao.getUsersData();
       
    }

	

}
