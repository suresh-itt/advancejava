/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.service;

/*validation method*/
public class ValidatorService {
     
    
    public boolean isValidName(String name) 
    {
       
        if(name.isEmpty()) 
        {
            return false;
        }
        if(name.matches("[0-9]"))
        {
            return false;
        }
        
        return true;
    }

    public boolean isValidEmail(String email)
    {
       
        return !email.isEmpty()&& email.matches("^[a-zA-Z0-9.!#$%&�*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$");
    }
    public boolean isValidPassword(String password)
    {
       
        return !password.isEmpty()&& (password.length()>6);
    }
     public boolean isValidPhone(String phone)
    {
       
        return !phone.isEmpty()&& phone.length()>=10;
    }
}
