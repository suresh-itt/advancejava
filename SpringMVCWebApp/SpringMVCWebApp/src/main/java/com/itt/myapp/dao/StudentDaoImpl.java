/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.dao;

import com.itt.myapp.controller.HomeController;
import com.itt.myapp.domain.Login;
import com.itt.myapp.domain.Student;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


@Repository
@Qualifier("studentDao")
public class StudentDaoImpl implements StudentDao 
{
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
  @Autowired
  DataSource dataSource;
  
  @Autowired
  JdbcTemplate jdbcTemplate;
    
   
  /*Inserting into the table after checking all the fields are valid*/
    @Override
    public void register(Student student) 
    {
        
        String sql = "insert into student values(?, ?, ?, ?, ?, ?)";
    
        jdbcTemplate.update(sql, new Object[] {student.getEmail(),student.getFirstName(), student.getLastName(), student.getPassword(), student.getGender(),student.getPhone() });
    }
    /*Checks for credention of a valid login user*/
    @Override
    public Student validateUser(Login login) 
    {
    String sql = "select * from student where email='" + login.getUsername() + "' and password='" + login.getPassword()
    + "'";
    List<Student> student = jdbcTemplate.query(sql, new StudentMapper());
    return student.size() > 0 ? student.get(0) : null;
    }
/*Display the table data using list*/
    @Override
    public List<Student> getUsersData() {
     String sql = "select * from student ";
        List<Student> student = jdbcTemplate.query(sql, new StudentMapper());
        return student;  
    }
  }
/*getting the data form the table using the row mapper class*/
  class StudentMapper implements RowMapper<Student> 
  {
  @Override
  public Student mapRow(ResultSet rs, int arg1) throws SQLException {
    Student student = new Student();
    student.setFirstName(rs.getString("firstname"));
    student.setLastName(rs.getString("lastname"));
    student.setPassword(rs.getString("password"));
    student.setEmail(rs.getString("email"));
    student.setPhone(rs.getString("phone"));
    student.setGender(rs.getString("gender"));
    return student;
  }
}
