<%-- 
    Document   : register
    Created on : Aug 10, 2017, 3:51:16 PM
    Author     : piyush.tiwari
--%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
            <title>Registration</title>
          
                <style type="text/css" rel="stylesheet">
                body
                {
                  background:#d2eff2;
                }
                form
                {
                  margin-left: 35%;
                  margin-top: 40px;
                  font-size: 15px;
                }

                .field
                {
                  width: 75%;
                  height: 20px;
                  font-size: 15px;
                  border-radius: 3px;
                }
                header
                {
                  text-align: center;
                  font-family: cursive;
                  color: blue;
                  margin-top: 20px;
                  font-weight: normal;
                  text-shadow: 2px 1px #2810A2;
                  font-size: 30px;
                }
                .creating a:hover
                {
                  color: red; 
                }
                .forsize
                {
                  background-color:#1A44EF;
                  width: 60%;
                  height: 30px;
                  font-size: 15px;
                  text-decoration: none;
                }
                .regdetails
                {
                  border: 2px #6DF3FC groove;
                  height: 500px;
                  width: 35%;
                  padding: 20px 10px 0px 80px;
                  border-radius: 15px;
                  background-color: rgba(233,70,74,0.1);
                }
                form .single
                {
                  font-size: 10px;
                  height: -20px;
                }
            </style>
                
           
        </head>
        <body>
            <header>
            <h1>Register here</h1>
        </header>
        <form action="registerProcess" method="post" modelAttribute="student">  
           <div class="regdetails">
                First name<br>
                <input class="field" path="firstName" type="text" name="firstName" id="firstName" required /><label style="color:red">${nameErrorMessage}</label>
                <br><br>
                Last name<br>
                 <input class="field" type="text" name="lastName" required />
                <br><br>
                E-mail/username<br>
                 <input class="field" type="email" name="email" required /><label style="color:red">${emailErrorMessage}</label>
                <br><br>
                Password<br>
                <input class="field" type="password" name="password" required /><label style="color:red">${passwordErrorMessage}</label>
                <br><br>
                
                Phone no<br>
                 <input class="field" type="number" name="phone" required /><label style="color:red">${phoneErrorMessage}</label>
                <br><br>
                Sex<br>
                <input type="radio" name="gender" value="male" required /> Male<br>
                <input type="radio" name="gender" value="female" required /> Female<br>
                <br>
               
                <input class="forsize" type="submit" value="SignUp" /><br>
                <div class="creating">
                  <p><a href="index.html">having an account? log in here</a></p>
                </div>
            </div>
        </form> 
        </body>
    </html>
