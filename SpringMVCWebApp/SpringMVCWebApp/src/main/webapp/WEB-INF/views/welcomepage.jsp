<%-- 
    Document   : welcomepage
    Created on : Aug 13, 2017, 5:04:50 PM
    Author     : suresh.sen
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>welcome</title>
    </head>
    <body>
        <h1 align="center">${message}</h1>
        <p> </p>
        <h2 align="center">Student List</h2>  
        <table border="2" width="70%" cellpadding="2" align="center">  
            <tr><th>First Name</th><th>Last Name</th><th>Gender</th><th>email</th><th>phone</th></tr>  
            <c:forEach var="stud" items="${list}">   
                <tr>  
                    <td>${stud.firstName}</td>  
                    <td>${stud.lastName}</td>  
                    <td>${stud.gender}</td>  
                    <td>${stud.email}</td>  
                    <td>${stud.phone}</td>  
                </tr>  
            </c:forEach>  
        </table>  
        <br/>  
        <a href="register">Add New student</a>
    </body>
</html>
