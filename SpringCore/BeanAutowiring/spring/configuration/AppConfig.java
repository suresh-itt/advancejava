package com.websystique.spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.websystique.spring.domain.MobileProcessor;
import com.websystique.spring.domain.Samsung;
import com.websystique.spring.domain.Snapdargon;

@Configuration
//@ComponentScan(basePackages="com.websystique.spring.domain")
public class AppConfig {
	@Bean
	public Samsung getPhoneSpecification()
	{
		return new Samsung();
	}
	@Bean
	public MobileProcessor getProcessor()
	{
		return new Snapdargon();
	}
	

}

