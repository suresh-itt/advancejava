package com.websystique.spring.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

//@Component 
public class Samsung
{
	@Autowired
	//@Qualifier("mediaTek")
	
	MobileProcessor process;
	
	//public MobileProcessor getProcessor() {
	//	return process;
	//}

	//public void setProcessor(MobileProcessor processor) {
		//this.process = processor;
	//}

	public void specification() 
	{
		System.out.println("4gb RAM,12Mp Camera,5000MAH Battery");
		process.processor();
	}

}
