package com.websystique.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.websystique.spring.AppConfig;

public class AppTest {

	public static void main(String[] args) {
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		
		System.out.println("----------Singleton Bean----------");
		SingletonBean singletonMessage = (SingletonBean)context.getBean(SingletonBean.class);
		System.out.println("id:"+singletonMessage.getId());
	    singletonMessage = (SingletonBean)context.getBean(SingletonBean.class);
		System.out.println("id:"+singletonMessage.getId());
		
		System.out.println("----------Protype Bean----------");
		PrototypeBean prototypeMessage = (PrototypeBean)context.getBean(PrototypeBean.class);
		System.out.println("id:"+prototypeMessage.getId());
		prototypeMessage = (PrototypeBean)context.getBean(PrototypeBean.class);
		System.out.println("id:"+prototypeMessage.getId());

	}

}
