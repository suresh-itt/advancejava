package com.websystique.spring;

public class SingletonBean {
	private int id;
	public SingletonBean()
	{
		id = NumberGenerator.getUniqueNumber();
	}
	public int getId() {
		return id;
	}
	
}
