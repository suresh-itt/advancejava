package com.websystique.spring;

import java.util.concurrent.atomic.AtomicInteger;

public class NumberGenerator {
	private static AtomicInteger uniqueNumber = new AtomicInteger(100);
	public static int getUniqueNumber()
	{
		return uniqueNumber.incrementAndGet();
	}

}
