package com.websystique.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
@Component
public class DemoPostProcessor implements BeanPostProcessor {

	public Object postProcessAfterInitialization(Object arg0, String arg1) throws BeansException {
		
		System.out.println("Processing a bean instance after initialization (Just after init life cycle)"+arg1);
		return arg0;
	}

	public Object postProcessBeforeInitialization(Object arg0, String arg1) throws BeansException {
		System.out.println("Processing a bean instance before initialization (after spring initatiate and Just brfore init life cycle)"+arg1);		
		return arg0;
	}

}
