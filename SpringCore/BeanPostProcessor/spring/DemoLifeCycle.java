package com.websystique.spring;

import org.springframework.stereotype.Component;
import javax.annotation.*;
@SuppressWarnings("restriction")
@Component
public class DemoLifeCycle {
	
	public void greetings()
	{
		System.out.println("Welcome to demonastartion of bean life cycle");
	}
	@PostConstruct
	public void init()
	{
		System.out.println("Bean is going through init method");
	}
	@PreDestroy
	public void  destroy()
	{
		System.out.println("Bean is going through Destory method");
		
	}

}
