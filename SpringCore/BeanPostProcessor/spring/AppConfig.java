package com.websystique.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
@Configuration
//@ComponentScan("com.websystique.spring")
public class AppConfig {
	@Bean
	public DemoLifeCycle getLifeObj() {
		return new DemoLifeCycle();
	}
	
	@Bean
	public DemoPostProcessor getPostObj() {
		return new DemoPostProcessor();
	}
 
}
