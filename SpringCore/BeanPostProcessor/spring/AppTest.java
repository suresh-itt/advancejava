package com.websystique.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.websystique.spring.AppConfig;

public class AppTest {

	public static void main(String[] args) {
		
	     AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
	     ctx.register(AppConfig.class);
	     ctx.refresh();
	     DemoLifeCycle service = ctx.getBean(DemoLifeCycle.class);
	     service.greetings();
	     DemoPostProcessor post = ctx.getBean(DemoPostProcessor.class);
	     ctx.registerShutdownHook();
		

	}

}
