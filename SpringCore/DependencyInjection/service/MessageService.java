package com.java.dependencyinjection.service;

public interface MessageService 
{
	boolean sendMessage(String msg, String rec);

}
