package com.java.dependencyinjection.test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.java.dependencyinjection.Configuration.DIConfiguration;
import com.java.dependencyinjection.consumer.MyDIApplication;

public class ClienApplication 
{
	public static void main(String[] args)
	{
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DIConfiguration.class);
		MyDIApplication app = context.getBean(MyDIApplication.class);
		
		String Message = "Hi suresh";
		String Email = "Suresh@abc.com";
		app.processMessage(Message,Email);
		
		//close the context
		context.close();
     }
}
