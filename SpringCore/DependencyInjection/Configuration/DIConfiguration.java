package com.java.dependencyinjection.Configuration;

import com.java.dependencyinjection.service.EmailService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.java.dependencyinjection.service.MessageService;

@Configuration
@ComponentScan(value={"com.java.dependencyinjection.consumer"})
public class DIConfiguration 
{
	@Bean
	public MessageService getMessageService()
	{
		return new EmailService();
	}
}
